<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama Perusahaan</th>
            <th>Alamat Perusahaan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $key => $d)
            <tr>
                <td>{{ $d->id }}</td>
                <td>{{ $d->nama }}</td>
                <td>{{ $d->alamat }}</td>
            </tr>
        @endforeach
    </tbody>
</table>