<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Models\Employee;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\EmployeeExport;

class EmployeeController extends Controller
{
   public function index()
   {
      $employee = Employee::all();

       return $employee->toJson();
   }

   public function store(Request $request)
   {
       $validatedData = $request->validate([
         'nama' => 'required',
         'atasan_id' => 'required',
         'company_id' => 'required',
       ]);

       $project = Employee::create([
         'nama' => $validatedData['nama'],
         'atasan_id' => $validatedData['atasan_id'],
         'company_id' => $validatedData['company_id'],

       ]);

       $msg = [
           'success' => true,
           'message' => 'Employee created successfully!'
       ];

       return response()->json($msg);
   }

   public function getEmployee($id) // for edit and show
   {
       $Employee = Employee::find($id);

       return $Employee->toJson();
   }

   public function update(Request $request, $id)
   {
       $validatedData = $request->validate([
         'nama' => 'required',
         'atasan_id' => 'required',
         'company_id' => 'required',
       ]);

       $Employee = Employee::find($id);
       $Employee->nama = $validatedData['nama'];
       $Employee->atasan_id = $validatedData['atasan_id'];
       $Employee->company_id = $validatedData['company_id'];

       $Employee->save();

       $msg = [
           'success' => true,
           'message' => 'Employee updated successfully'
       ];

       return response()->json($msg);
   }

   public function delete($id)
   {
       $Employee = Employee::find($id);
       if(!empty($Employee)){
           $Employee->delete();
           $msg = [
               'success' => true,
               'message' => 'Employee deleted successfully!'
           ];
           return response()->json($msg);
       } else {
           $msg = [
               'success' => false,
               'message' => 'Employee deleted failed!'
           ];
           return response()->json($msg);
       }
   }

    public function export_excel() {
        return Excel::download(new EmployeeExport, 'Employee.xlsx');
    }

    public function export_pdf() {
        return Excel::download(new EmployeeExport, 'Employee.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }
}

