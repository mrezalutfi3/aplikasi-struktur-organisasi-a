import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import EmployeeIndex from './EmployeeIndex'
import EmployeeCreate from './EmployeeCreate'
import EmployeeShow from './EmployeeShow'
import EmployeeEdit from './EmployeeEdit'
 
class App extends Component {
    render () {
        return (
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                    <Route exact path='/' component={EmployeeIndex}/>
                    <Route exact path='/create' component={EmployeeCreate} />
                    <Route path='/employee/edit/:id' component={EmployeeEdit} />
                    <Route path='/employee/:id' component={EmployeeShow} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}
 
ReactDOM.render(<App />, document.getElementById('app'))