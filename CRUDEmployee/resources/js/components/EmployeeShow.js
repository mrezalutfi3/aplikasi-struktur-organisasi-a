import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
 
    class EmployeeShow extends Component {
      constructor (props) {
        super(props)
        this.state = {
          employee: {}
        }
      }
 
      componentDidMount () {
 
        const employeeId = this.props.match.params.id
 
        axios.get(`/api/employee/${employeeId}`).then(response => {
          this.setState({
            employee: response.data
          })
        })
      }
 
      render () {
        const { employee } = this.state
 
        return (
          <div className='container py-4'>
            <div className='row justify-content-center'>
              <div className='col-md-8'>
                <div className='card'>
                  <div className='card-header'>Judul Artikel: {employee.title}</div>
                  <div className='card-body'>
                    <p>{employee.content}</p>
                    <Link
                        className='btn btn-primary'
                        to={`/`}
                        >Back
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
      }
    }
 
export default EmployeeShow