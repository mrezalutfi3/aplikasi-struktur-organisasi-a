import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';
 
    class EmployeeEdit extends Component {
      constructor (props) {
        super(props)
        this.state = {
            nama: '',
            atasan_id: '',
            company_id: '',
          alert: null,
          message:'',
          errors: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleUpdateemployee = this.handleUpdateemployee.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
      }
 
      handleFieldChange (event) {
        this.setState({
          [event.target.name]: event.target.value
        })
      }
 
      componentDidMount () {
 
        const employeeId = this.props.match.params.id
 
        axios.get(`/api/employee/edit/${employeeId}`).then(response => {
          this.setState({
            nama: response.data.nama,
            atasan_id: response.data.atasan_id,
            company_id:  response.data.company_id
          })
        })
      }
 
      goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                company_id="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke Siap"
                >
                {this.state.message}
            </SweetAlert>
          );
          this.setState({
            alert: getAlert()
          });
      }
 
      onSuccess() {
        this.props.history.push('/');
      }
 
      hideAlert() {
        this.setState({
          alert: null
        });
      }
 
      handleUpdateemployee (event) {
        event.preventDefault()
 
        const employee = {
            nama: this.state.nama,
          atasan_id: this.state.atasan_id,
          company_id: this.state.company_id,
        }
 
        const employeeId = this.props.match.params.id
 
        axios.put(`/api/employee/${employeeId}`, employee)
          .then(response => {
            // redirect to the homepage
            var msg = response.data.success;
            if(msg == true){
                this.setState({
                    message: response.data.message
                })
                return this.goToHome();
            }
 
          });
      }
 
      hasErrorFor (field) {
        return !!this.state.errors[field]
      }
 
      renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
          return (
            <span className='invalid-feedback'>
              <strong>{this.state.errors[field][0]}</strong>
            </span>
          )
        }
      }
 
      
      render () {
        const { employee } = this.state
        return (
          <div className='container py-4'>
            <div className='row justify-atasan_id-center'>
              <div className='col-md-6'>
                <div className='card'>
                  <div className='card-header'>Create new project</div>
                  <div className='card-body'>
                    <form onSubmit={this.handleUpdateemployee}>
                      <div className='form-group'>
                        <label htmlFor='nama'>Nama</label>
                        <input
                          id='nama'
                          type='text'
                          className={`form-control ${this.hasErrorFor('nama') ? 'is-invalid' : ''}`}
                          name='nama'
                          value={this.state.nama}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('nama')}
                      </div>
                      <div className='form-group'>
                        <label htmlFor='atasan_id'>Project atasan_id</label>
                        <input
                          id='atasan_id'
                          className={`form-control ${this.hasErrorFor('atasan_id') ? 'is-invalid' : ''}`}
                          name='atasan_id'
                          rows='10'
                          value={this.state.atasan_id}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('atasan_id')}
                      </div>
                      <div className='form-group'>
                        <label htmlFor='company_id'>company_id</label>
                        <input
                          id='company_id'
                          type='text'
                          className={`form-control ${this.hasErrorFor('company_id') ? 'is-invalid' : ''}`}
                          name='company_id'
                          value={this.state.company_id}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('company_id')}
                      </div>
                      <Link
                        className='btn btn-secondary'
                        to={`/`}
                        >Back
                      </Link>
                      <button className='btn btn-primary'>Update</button>
                      {this.state.alert}
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        )
      }
    }
export default EmployeeEdit