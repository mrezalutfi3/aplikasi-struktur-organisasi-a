import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SweetAlert from 'react-bootstrap-sweetalert';
 
class EmployeeCreate extends Component {
     
    constructor (props) {
        super(props)
        this.state = {
            nama: '',
            atasan_id: '',
            company_id: '',
            alert: null,
            errors: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewemployee = this.handleCreateNewemployee.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }
 
    handleFieldChange (event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
 
    goToHome(){
        const getAlert = () => (
            <SweetAlert
                success
                title="Success!"
                onConfirm={() => this.onSuccess() }
                onCancel={this.hideAlert()}
                timeout={2000}
                confirmBtnText="Oke Siap"
                >
                Created employee successfully
            </SweetAlert>
        );
        this.setState({
            alert: getAlert()
        });
    }
 
    onSuccess() {
        this.props.history.push('/');
    }
 
    hideAlert() {
        this.setState({
            alert: null
        });
    }
 
    handleCreateNewemployee (event) {
        event.preventDefault()
        const employee = {
          nama: this.state.nama,
          atasan_id: this.state.atasan_id,
          company_id: this.state.company_id

        }
        axios.post('/api/employee/store', employee).then(response => { 
            var msg = response.data.success;
            if(msg == true){
                return this.goToHome();
            }
        })
    }
 
    hasErrorFor (field) {
        return !!this.state.errors[field]
    }
 
    renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
            return (
            <span className='invalid-feedback'>
                <strong>{this.state.errors[field][0]}</strong>
            </span>
            )
        }
    }
 
    render () {
        return (
        <div className='container py-4'>
            <div className='row justify-content-center'>
              <div className='col-md-6'>
                <div className='card'>
                  <div className='card-header'>Create new project</div>
                  <div className='card-body'>
                    <form onSubmit={this.handleCreateNewemployee}>
                      <div className='form-group'>
                        <label htmlFor='title'>Nama</label>
                        <input
                          id='nama'
                          type='text'
                          className={`form-control ${this.hasErrorFor('nama') ? 'is-invalid' : ''}`}
                          name='nama'
                          value={this.state.nama}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('nama')}
                      </div>
                      <div className='form-group'>
                        <label htmlFor='content'>Atasan</label>
                        <textarea
                          id='atasan_id'
                          className={`form-control ${this.hasErrorFor('atasan_id') ? 'is-invalid' : ''}`}
                          name='atasan_id'
                          rows='10'
                          value={this.state.atasan_id}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('atasan_id')}
                      </div>
                      <div className='form-group'>
                        <label htmlFor='content'>Company</label>
                        <textarea
                          id='company_id'
                          className={`form-control ${this.hasErrorFor('company_id') ? 'is-invalid' : ''}`}
                          name='company_id'
                          rows='10'
                          value={this.state.company_id}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('company_id')}
                      </div>
                      <Link
                        className='btn btn-secondary'
                        to={`/`}
                        >Back
                      </Link>
                      <button className='btn btn-primary'>Create</button>
                      {this.state.alert}
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
    }
}
export default EmployeeCreate