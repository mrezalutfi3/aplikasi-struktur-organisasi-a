<!DOCTYPE html>
<html>
<head>
    <title>CRUD Employee</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
</head>
<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>CRUD Employee</h4>
            </div>
            <div class="panel-body">
                <form action="{{url('create')}}" method="get">
                    <div class="form-group">
                        <input type="submit" name="new" id="new" value="Entry Baru" class="btn btn-success">
                    </div>
                </form>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>nama</th>
                            <th>atasan_id</th>
                            <th>company_id</th>
                            <th>action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($data as $key => $d)
                        <tr>
                            <td>{{ $d->id }}</td>
                            <td>{{ $d->nama }}</td>
                            <td>{{ $d->atasan_id }}</td>
                            <td>{{ $d->company_id }}</td>
                            <td>
                                <a href="{{url('read',array($d->id))}}">Read</a>
                                <a href="{{url('delete',array($d->id))}}">Delete</a>
                                <a href="{{url('edit',array($d->id))}}">Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>