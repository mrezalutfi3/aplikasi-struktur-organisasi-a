<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama Karyawan</th>
            <th>ID Atasan</th>
            <th>ID Perusahaan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($employee as $key => $d)
            <tr>
                <td>{{ $d->id }}</td>
                <td>{{ $d->nama }}</td>
                <td>{{ $d->atasan_id }}</td>
                <td>{{ $d->company_id }}</td>
            </tr>
        @endforeach
    </tbody>
</table>