<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [EmployeeController::class, 'index']);
// Route::get('back', [EmployeeController::class, 'back']);
// Route::get('create', [EmployeeController::class, 'create']);
// Route::post('insert', [EmployeeController::class, 'insert']);
// Route::get('delete/{id}', [EmployeeController::class, 'delete']);
// Route::get('edit/{id}', [EmployeeController::class, 'edit']);
// Route::post('update/{id}', [EmployeeController::class, 'update']);
// Route::get('read/{id}', [EmployeeController::class, 'read']);

Route::view('/employees', 'app');
Route::view('/employee/edit/{id}', 'app');
Route::view('/employee/{id}', 'app');
Route::view('/', 'app');
Route::view('/{path}', 'app');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
