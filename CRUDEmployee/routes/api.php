<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/data', [EmployeeController::class, 'getData']);
Route::get('/employees', [EmployeeController::class, 'index']);
Route::post('/employee/store',[EmployeeController::class,'store']);
Route::get('/employee/edit/{id}',[EmployeeController::class,'getEmployee']);
Route::get('/employee/{id}', [EmployeeController::class,'getEmployee']);
Route::put('/employee/{id}', [EmployeeController::class,'update']);
Route::delete('/employee/delete/{id}', [EmployeeController::class,'delete']);
Route::get('/export_excel', [EmployeeController::class, 'export_excel']);
Route::get('/export_pdf', [EmployeeController::class, 'export_pdf']);
